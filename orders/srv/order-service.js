const cds = require("@sap/cds")

module.exports = cds.service.impl(async (srv) => {
  const { Customers } = srv.entities

  const customerService = await cds.connect.to("CustomerService")

  srv.on("READ", Customers, async (req) => {
    try {
      return await customerService.run(req.query)
    } catch (err) {
      return req.reject(err)
    }
  })

})