using { Orders as dbOrders } from '../db/schema';

using { CustomerService as external } from './external/CustomerService';

@path: 'service/order'
service OrderService {
  entity Orders as projection on dbOrders;
  annotate Orders with @odata.draft.enabled;

  entity Customers as projection on external.Customers;

  action doAction(input1: String, input2: String) returns String;
}