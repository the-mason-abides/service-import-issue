/* checksum : 03099e333f4cd6b58c9f18a48ef6d324 */
@cds.external : true
@cds.persistence.skip : true
@Common.DraftRoot : {
  $Type: 'Common.DraftRootType',
  ActivationAction: 'CustomerService.draftActivate',
  EditAction: 'CustomerService.draftEdit',
  PreparationAction: 'CustomerService.draftPrepare'
}
entity CustomerService.Customers {
  key ID : UUID not null;
  customerNumber : Integer;
  customerName : String(60);
  @UI.Hidden : true
  key IsActiveEntity : Boolean not null default true;
  @UI.Hidden : true
  HasActiveEntity : Boolean not null;
  @UI.Hidden : true
  HasDraftEntity : Boolean not null;
  @cds.ambiguous : 'missing on condition?'
  @UI.Hidden : true
  DraftAdministrativeData : Association to one CustomerService.DraftAdministrativeData {  };
  @cds.ambiguous : 'missing on condition?'
  SiblingEntity : Association to one CustomerService.Customers {  };
} actions {
  action draftPrepare(
    ![in] : $self,
    SideEffectsQualifier : LargeString
  ) returns CustomerService.Customers;
  action draftActivate(
    ![in] : $self
  ) returns CustomerService.Customers;
  action draftEdit(
    ![in] : $self,
    PreserveChanges : Boolean
  ) returns CustomerService.Customers;
};

@cds.external : true
@cds.persistence.skip : true
@Common.Label : '{i18n>Draft_DraftAdministrativeData}'
entity CustomerService.DraftAdministrativeData {
  @UI.Hidden : true
  @Common.Label : '{i18n>Draft_DraftUUID}'
  key DraftUUID : UUID not null;
  @odata.Precision : 7
  @odata.Type : 'Edm.DateTimeOffset'
  @Common.Label : '{i18n>Draft_CreationDateTime}'
  CreationDateTime : Timestamp;
  @Common.Label : '{i18n>Draft_CreatedByUser}'
  CreatedByUser : String(256);
  @UI.Hidden : true
  @Common.Label : '{i18n>Draft_DraftIsCreatedByMe}'
  DraftIsCreatedByMe : Boolean;
  @odata.Precision : 7
  @odata.Type : 'Edm.DateTimeOffset'
  @Common.Label : '{i18n>Draft_LastChangeDateTime}'
  LastChangeDateTime : Timestamp;
  @Common.Label : '{i18n>Draft_LastChangedByUser}'
  LastChangedByUser : String(256);
  @Common.Label : '{i18n>Draft_InProcessByUser}'
  InProcessByUser : String(256);
  @UI.Hidden : true
  @Common.Label : '{i18n>Draft_DraftIsProcessedByMe}'
  DraftIsProcessedByMe : Boolean;
};

@cds.external : true
service CustomerService {};

