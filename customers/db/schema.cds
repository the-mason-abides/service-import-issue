using { cuid } from '@sap/cds/common';

entity Customers: cuid {
  customerNumber: Integer;
  customerName: String(60);
}