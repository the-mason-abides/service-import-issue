using { Customers as dbCustomers } from '../db/schema';

@path: 'service/customer'
service CustomerService {
  entity Customers as projection on dbCustomers;
  annotate Customers with @odata.draft.enabled;
}